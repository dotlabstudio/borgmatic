Checking Backups
To see all of your backup archives you can run:

sudo borgmatic list

To see details about usage and the size of archives you can run:

sudo borgmatic info
