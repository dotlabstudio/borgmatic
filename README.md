#borgmatic for automated backups using the borg backup software

install the backup software on openSUSE micro os server

```
sudo transactional-update pkg install borgbackup borgmatic
```

reboot the server

generate config file - if one does not exist in /etc/borgmatic/config.yaml

```
sudo generate-borgmatic-config
```

generate new ssh keys

```
ssh-keygen -t ed25519 -a 100
```

for borgbase

```
cat ~/.ssh/borg_id_ed25519.pub
```

Copy this public key and add it to your BorgBase account by clicking "ACCOUNT" and then "ADD KEY".

Create Repository in BorgBase
Give it a name you'll recognise for your server and add the new key 

from the borgbase setup for the repo
initialise the repo with the passphrase here

Initializing a repository sets up the encryption and makes it ready for use. Different algorithms and key modes are available. For Borg 1.1+ repokey mode with blake2 is the best option.

```
$ borg init -e repokey-blake2 ssh://v42kxxw4@v42kxxw4.repo.borgbase.com/./repo
```

above as an example - change ssh://repo as described in the borgbase repo

Please use the repository URL exactly as shown above or as copied from the repository table. It's not necessary to change any part of it, including the final repo part.

If you have already set up Borgmatic and added the repo URL to its config, you can initialize the repo using:

```
$ borgmatic init -e repokey-blake2
```

When first connecting, you may be asked to validate the host key fingerprint. This ensures you are connecting to the correct server. This fingerprint will be validated before all subsequent connections. The fingerprints for your current repo are: {will list a brunch of keys}

Be sure to keep your chosen passphrase (and keyfile when using keyfile mode) in a safe place. Without it, the data will be inaccessible.

then create backups

Now we are ready for our first backup. You'll need to choose a name for your archive and a local folder to back up.

```
$ borg create ssh://v42kxxw4@v42kxxw4.repo.borgbase.com/./repo:: {hostname}-{user}-{now:%Y-%m-%d-%H:%M:%S} my-folder
```

The create command has many options, like compressing data. See the official docs for more.

In most cases, it will be easier to manage your backups via Borgmatic. It will keep settings, like repo URL, compression, source directories and passphrase in one place and greatly simplify running commands. You find a customized config file in the next step. After adding a Borgmatic config file, creating a backup is as simple as:

Editing the Config File
Open /etc/borgmatic/config.yaml by running 

```
sudo vim /etc/borgmatic/config.yaml
```

and edit its contents to look something like the sample config.yaml file in this repo.

Borgmatic keeps your Borg settings in one place, manages pruning of old archives and can run custom commands before- and after backup runs. Simply copy the pre-filled template below to /etc/borgmatic/config.yaml or ~/.config/borgmatic/config.yaml and adjust as needed. See the official docs for all possible options.





