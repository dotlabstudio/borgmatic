#Borgmatic keeps your Borg settings in one place, manages pruning of old archives and can run custom commands before- and after backup #runs.
#
#Simply copy the pre-filled template below to /etc/borgmatic/config.yaml or ~/.config/borgmatic/config.yaml and adjust as needed. 
#
#See the official docs for all possible options.

location:
    source_directories:
        - /var/DATA
        - /etc
        - /home

    repositories:
        - ssh://<borgrepo>.repo.borgbase.com/./repo

    exclude_patterns:
        - '*.pyc'
        - ~/*/.cache

    exclude_if_present: .nobackup
    one_file_system: true

storage:
    compression: auto,zstd
    encryption_passphrase: "<passphrase>"
    archive_name_format: '{hostname}-{now:%Y-%m-%d-%H:%M:%S}'

    ssh_command: ssh -i /home/<username>/.ssh/id_ed25519

    # Needs recent Borgmatic version
    retries: 5
    retry_wait: 5

retention:
    keep_daily: 3
    keep_weekly: 4
    keep_monthly: 12
    keep_yearly: 2
    prefix: '{hostname}-'

consistency:
    checks:
        # Uncomment to always do integrity checks.
        # (takes long time for larger repos)
        - repository
        - archives

    check_last: 3
    prefix: '{hostname}-'

hooks:
    # Shell commands to execute before or after a backup
    before_backup:
        - echo "`date` - Starting backup"

    after_backup:
        - echo "`date` - Finished backup"
